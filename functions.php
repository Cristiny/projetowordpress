<?php 
function add_styles_and_scripts(){
    global $template;

    wp_enqueue_style( "reset-sheet", get_template_directory_uri() . "/css/reset.css");
    wp_enqueue_style( "style-sheet", get_template_directory_uri() . "/style.css");
    wp_enqueue_script("mail-go-script", get_template_directory_uri()."/assets/js/mailgo.min.js");
}
add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );
?>